const express = require('express')
const router = express.Router()
const userController = require('../controllers/userControllers')
const auth = require('../auth')


router.post('/checkEmail', (request,response) => {
	userController.checkEmailExists(request.body).then(resultFromController => response.send(
		resultFromController))
})

router.post('/register', (request,response) =>{
	userController.registerUser(request.body).then(resultFromController => response.send(
		resultFromController))
})


router.post('/login', (request,response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(
		resultFromController)) 
})


router.get('/details', auth.verify, (request,response) => {

	const userData = auth.decode(request.headers.authorization)
	// console.log(userData)
	const isAdminData = auth.decode(request.headers.authorization).isAdmin
	// console.log(isAdminData)
	console.log(request.headers.authorization)


	userController.getProfile({id: userData.id}).then((resultFromController) => response.send(resultFromController))
})


router.post("/enroll", auth.verify, (request, response) => {

	
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		courseId: request.body.courseId
	}

	userController.enroll(data).then(resultFromController => response.send(resultFromController))

});



module.exports = router;