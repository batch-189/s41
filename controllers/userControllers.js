const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Course = require('../models/Course')

module.exports.checkEmailExists = (reqBody) => {
	return User.find({id: reqBody.id}).then(result => {
		if (result.length > 0) {
			return result
		}else{
			return false
		}
	}) 
}

//user Registratin


module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10) 
		//hashSync(<datatoBeHash>,<saltRound>)

	})
	return newUser.save().then((user,error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

//User Authentication
module.exports.loginUser = (reqBody) => {
	

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		}else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			//compareSync(<data>, <encryptedPassword>)
				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)}
				}else {
					return false
				}

		}
	})
}

module.exports.getProfile = (dataId) => {
	console.log(dataId)
	return User.findOne(dataId).then(result => {
		console.log(result)
		if(result == null){
			return false
		}else{
			result.password = ""
			return result
		}
	})
} 



module.exports.enroll = async (data) => {
	
	if(data == data.isAdmin){
		return false
	}

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});
		return user.save().then((result,error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId})

		return course.save().then((result,error) => {
			
			if(error){
				return false
			}else{
				return true
			}
		})
	})



	if(isUserUpdated && isCourseUpdated){
		return true
	}else {
		return false
	}
}

